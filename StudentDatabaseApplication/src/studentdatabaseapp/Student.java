package studentdatabaseapp;

import java.util.Scanner;

/**
 * Created by blair on 07/05/18.
 */
public class Student {

    private String firstName;
    private String lastName;
    private String gradeYear;
    private String studentID;
    private String courses = "";
    private int tuitionBalance = 0;
    private static int costOfCourse = 600;
    private static int id = 1000;

    public Student() {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter student first name: ");
        this.firstName = input.nextLine();

        System.out.println("Enter student last name: ");
        this.lastName = input.nextLine();

        System.out.println("1 - Freshman\n2 - Sophmore\n3 - Junior\n4 - Senior\nEnter student class level: ");
        this.gradeYear = input.nextLine();

        setStudentID();
    }

    private void setStudentID() {

        id++;
        this.studentID = gradeYear + "" + id;
    }

    public void enroll() {
        do {
            System.out.println("Enter course to enroll (Q to quit): ");
            Scanner in = new Scanner(System.in);
            String course = in.nextLine();

            if (!course.equals("Q")) {
                courses = courses + "\n" + course;
                tuitionBalance = tuitionBalance + costOfCourse;
            }
            else {break;}
        }while (1 != 0);
        System.out.println("Tuition balance: £" + tuitionBalance);
    }

    public void viewBalance() {
        System.out.println("Your balance is: £ " + tuitionBalance);
    }

    public void payTuition() {
        viewBalance();
        System.out.println("Enter your payment: £");
        Scanner in = new Scanner(System.in);
        int payment = in.nextInt();
        tuitionBalance = tuitionBalance - payment;
        System.out.println("Thank you for your payment of £" + payment);
        viewBalance();
    }

    public String toString(){
        return "Name: " + firstName + " " + lastName + "\nGrade Level " + gradeYear + "\nStudent ID: " + studentID + "\nCourses Enrolled: " + courses + "\nBalance: £" + tuitionBalance;
    }

}
