package studentdatabaseapp;

import java.util.Scanner;

/**
 * Created by blair on 07/05/18.
 */
public class StudentDatabaseApp {

    public static void main(String[] args){

        System.out.println("Enter number of students to enroll: ");
        Scanner input = new Scanner(System.in);
        int numOfStudents = input.nextInt();

        Student [] students = new Student[numOfStudents];

        for(int n = 0; n < numOfStudents; n++){
            students[n] = new Student();
            students[n].enroll();
            students[n].payTuition();
            System.out.println(students[n].toString());
        }
    }
}
